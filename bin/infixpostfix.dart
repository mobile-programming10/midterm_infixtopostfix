import 'dart:io';
import 'package:stack/stack.dart'; // install package stack
import 'dart:math';

//เปลี่ยนค่าInput เป็น Infix
List splitAtoInfix(String str) {
  List<String> x = str.split(""); //Return list ของ string ที่ถูก Split
  return x;
}

// ที่ไม่ต้องการดำเนินการ
bool isNotOpd(String c) {
  return c.contains(new RegExp(r'[0-9a-zA-Z]')); //ใช้สำหรับหาว่ามี เซตของตัวอักษร ที่ต้องการหรือไม่
}

//ทำ Infix เป็น Postfix
String? infixToPostfix(String n) {
  Stack operand = Stack();
  String postFix = "";

  for (int i = 0; i < n.length; i++) {
    String letter = n[i];
    if (isNotOpd(letter)) {  
      postFix += letter; 
    } else  if (letter == '(') {
      operand.push('(');
    } else if (letter == ')') {
      while (operand.top() != '(') {
        postFix += operand.top();
        operand.pop();
      }
      operand.pop();
    } else {
      while (operand.isNotEmpty && precedence(letter) <= precedence(operand.top())) {
        postFix += operand.top();
        operand.pop();
      }
      operand.push(letter);
    }
  }

  while (!operand.isEmpty) {
    postFix += operand.top();
    operand.pop();
  }
  return postFix;
}

//เช็คลำดับความสำคัญของเครื่องหมาย
int precedence(String ch) {
  if (ch == '^') {
    return 3;
  } else if (ch == '/' || ch == '*') {
    return 2;
  } else if (ch == '+' || ch == '-') {
    return 1;
  } else {
    return 0;
  }
}

//ตรวจสอบว่าเป็นตัวเลขหรือไม่
bool isNumeric(postfix) {
  return postfix.contains(new RegExp(r'^[0-9]+$'));
}

//หาผลรวมของ postfix
Evaluate(var postfix) {
  List<double> myList = [];
  for (var i = 0; i < postfix.length; i++) {
    if (isNumeric(postfix[i])) {
      double num = double.parse(postfix[i]);
      myList.add(num);
    } else {
      double Right;
      Right = myList.last;
      myList.removeLast();
      double Left;
      Left = myList.last;
      myList.removeLast();
      double sum = 0;

      switch (postfix[i]) {
        case '+':
          sum = Left + Right;
          break;
        case '-':
          sum = Left - Right;
          break;
        case '*':
          sum = Left * Right;
          break;
        case '/':
          sum = Left / Right;
          break;
        case '^':
          sum = (0.0) + pow(Left,Right);
          break;
      }
      myList.add(sum);
    }
  }
  return myList[0];
}

void main(List<String> arguments) {
  stdout.write("Input :");
  String a = stdin.readLineSync()!;
  stdout.write("Infix : ");
  print(splitAtoInfix(a));

  stdout.write("Postfix : ");
  String? sp = infixToPostfix(a) as String;
  print(splitAtoInfix(sp));

  stdout.write("The evaluate is : ");
  print(Evaluate(splitAtoInfix(sp)));
}
